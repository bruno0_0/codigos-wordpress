function site_register_form() {
    $complect_name = ( ! empty( $_POST['complect_name'] ) ) ? trim( $_POST['complect_name'] ) : '';
    $crm = ( ! empty( $_POST['crm'] ) ) ? trim( $_POST['crm'] ) : '';
    $sexo = ( ! empty( $_POST['sexo'] ) ) ? trim( $_POST['sexo'] ) : '';
    ?>
    <p>
        <label for="complect_name"><?php _e( 'Nome Completo' ) ?><br />
        <input type="text" name="complect_name" id="complect_name" class="input" value="<?php echo esc_attr( wp_unslash( $complect_name ) ); ?>" size="25" /></label>
    </p>

    <p>
        <label for="crm"><?php _e( 'CRM' ) ?><br />
        <input type="text" name="crm" id="crm" class="input" value="<?php echo esc_attr( wp_unslash( $crm ) ); ?>" size="25" /></label>
    </p>
    <p>
        <label for="sexo"><?php _e( 'Sexo' ) ?><br />
            <select type="text" name="sexo" id="sexo" class="input">
                <option value="masculino">Masculino</option>
                <option value="feminino">Feminino</option>
            </select>
        </label>
    </p>
    <?php
}
add_action( 'register_form', 'site_register_form' );



function site_registration_errors( $errors, $sanitized_user_login, $user_email ) {
    if ( empty( $_POST['complect_name'] ) || ! empty( $_POST['complect_name'] ) && trim( $_POST['complect_name'] ) == '' ) {
        $errors->add( 'first_name_error', __( '<strong>ERRO</strong>: Você precisa informar seu nome completo.', 'site' ) );
    }

    if ( empty( $_POST['crm'] ) || ! empty( $_POST['crm'] ) && trim( $_POST['crm'] ) == '' ) {
        $errors->add( 'crm_error', __( '<strong>ERRO</strong>: Você precisa informar seu CRM.', 'site' ) );
    }

    return $errors;
}
add_filter( 'registration_errors', 'site_registration_errors', 10, 3 );


function site_user_register( $user_id ) {
    if ( ! empty( $_POST['complect_name'] ) ) {
        update_user_meta( $user_id, 'complect_name', trim( $_POST['complect_name'] ) );
    }
    if ( ! empty( $_POST['crm'] ) ) {
        update_user_meta( $user_id, 'crm', trim( $_POST['crm'] ) );
    }
    if ( ! empty( $_POST['sexo'] ) ) {
        update_user_meta( $user_id, 'sexo', trim( $_POST['sexo'] ) );
    }
}
add_action( 'user_register', 'site_user_register' );