function remover_boxes() {
 remove_meta_box('postexcerpt', 'page', 'normal');
 remove_meta_box('authordiv', 'page', 'normal');
 remove_meta_box('postcustom', 'page', 'normal');
 remove_meta_box('commentstatusdiv', 'page', 'normal');
 remove_meta_box('commentsdiv', 'page', 'normal');
 remove_meta_box('authordiv', 'page', 'normal');
 remove_meta_box('authordiv', 'post', 'normal');
 remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
 remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
 remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
 remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
 remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
 remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
 remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
 remove_meta_box('dashboard_primary', 'dashboard', 'side');
 remove_meta_box('dashboard_secondary', 'dashboard', 'side');
 remove_meta_box('linktargetdiv', 'link', 'normal');
 remove_meta_box('linkadvanceddiv', 'link', 'normal');
 remove_meta_box('postexcerpt', 'post', 'normal');
 remove_meta_box('trackbacksdiv', 'post', 'normal');
 remove_meta_box('postcustom', 'post', 'normal');
 remove_meta_box('commentstatusdiv', 'post', 'normal');
 remove_meta_box('commentsdiv', 'post', 'normal');
 remove_meta_box('revisionsdiv', 'post', 'normal');
 remove_meta_box('authordiv', 'post', 'normal');
}
 add_action( 'admin_menu' , 'remover_boxes' );

//-------------------------------------------------------------------------//
//Para remover itens apenas para quem não é administrador:
//
//function remover_boxes() {
// if ( ! current_user_can( 'manage_options' ) ) {
//   remove_meta_box( 'postexcerpt' , 'page' , 'normal' );
// }
//}
//add_action( 'admin_menu' , 'remover_boxes' );
//Para remover itens apenas se o usuário for autor ou abaixo disso, usamos o if assim:
//
//if ( ! current_user_can( ‘delete_others_pages’ ) )
//
//Para limitar para administradores apenas, deixamos o if assim:
//
//if ( is_admin() ) {
//ou
//if ( current_user_can( ‘manage_options’ ) ) {

//-------------------------------------------------------------------------//
//CONHECENDO A FUNÇÃO
//Para isso, devemos conhecer a função remove_meta_box. O código segue essa estrutura:
//
//<?php remove_meta_box( $id, $page, $context ); ?>
//
//1. Em Id, diremos ao WordPress qual id do elemento html a ser removido. Exemplos:
//
//* 'authordiv' – box de autor
//* 'categorydiv' – box de categorias
//* 'commentstatusdiv' – box de discussão, status de comentários
//* 'commentsdiv' – box de comentários
//* 'formatdiv' – box de formatos
//* 'pageparentdiv' – box de atributos de página
//* 'postcustom' – box de custom fields
//* 'postexcerpt' – box de resumo
//* 'postimagediv' – box de imagem destacada
//* 'revisionsdiv' – box de revisões
//* 'slugdiv' – box de slug
//* 'submitdiv' – box de data, status e botões de salvar e atualizar
//* 'tagsdiv-post_tag' – box de tags
//* 'trackbacksdiv' – box de trackbacks
//* e muito mais
//
//Alguns exemplos para o dashboard:
//
//* 'dashboard_right_now' – “Agora”
//* 'dashboard_recent_comments' – Comentários recentes
//* 'dashboard_plugins' – Plugins
//* dashboard_quick_press' – Rascunho rápido
//* 'dashboard_recent_drafts' – Rascunhos recentes
//* 'dashboard_primary' – Novidades e eventos WordPress
//* 'dashboard_secondary' – Novidades do WordPress
//* 'dashboard_activity' – “Atividade”
//
//2. Em Page, iremos dizer em que tela esses boxes serão removidos. As opções são:
//
//* 'post' – post
//* 'page' – página
//* 'attachment' – anexos (mídia)
//* 'link' – link
//* 'dashboard' – dashboard, o Painel (tela inicial quando você abre o WordPress)
//* custom post types
//
//3. Em Context, diremos se queremos normal, advanced ou side. O padrão é advanced. Isso depende do 
//item e da tela onde será usado. Na dúvida, copie e colo como deixei nos exemplos na próxima sessão.