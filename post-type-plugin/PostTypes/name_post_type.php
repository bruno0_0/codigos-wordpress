<?php

class name_post_type
{

    static function init()
    {
        add_action('init', array(__CLASS__, 'custom_post_type'), 5);
        add_action('init', array(__CLASS__, 'create_product_taxonomies'), 0);
    }

    static function custom_post_type()
    {

        $labels = array(
            'name'                => _x('Produtos', 'template_name'),
            'singular_name'       => _x('Produtos', 'template_name'),
            'menu_name'           => __('Produtos'),
            'parent_item_colon'   => __('Produtos'),
            'all_items'           => __('Todos os produtos'),
            'view_item'           => __('Ver produtos'),
            'add_new_item'        => __('Adicionar Novo produto'),
            'add_new'             => __('Adicionar Novo'),
            'edit_item'           => __('Editar produtos'),
            'update_item'         => __('Atualizar produtos'),
            'search_items'        => __('Procurar produtos'),
            'not_found'           => __('Não encontrado'),
            'not_found_in_trash'  => __('Não encontrado na lixeira'),
        );


        $args = array(
            'label'               => __('Produto', 'template_name'),
            'description'         => __('Produtos notícias e resenhas', 'template_name'),
            'labels'              => $labels,
            'supports'            => array('title', 'thumbnail', 'custom-fields', 'editor', 'excerpt'),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => array('slug' => 'produtos'),
            'capabilities' => array(
                'edit_post' => 'edit_product',
                'edit_posts' => 'edit_products',
                'edit_others_posts' => 'edit_other_products',
                'edit_private_posts' => 'edit_private_products',
                'edit_published_posts' => 'edit_published_products',
                'publish_posts' => 'publish_products',
                'read_post' => 'read_product',
                'read_private_posts' => 'read_private_products',
                'delete_post' => 'delete_product',
                'delete_posts' => 'delete_products',
                'delete_private_posts' => 'delete_private_product',
                'delete_published_posts' => 'delete_published_product',
                'delete_others_posts' => 'delete_others_products',
                'read' => 'read_product',
                'create_posts' => 'create_product'
            ),
        );

        register_post_type('products', $args);
    }


    static function create_product_taxonomies()
    {
        $labels = array(
            'name'              => _x('Categorias', 'template_name'),
            'singular_name'     => _x('Categorias', 'template_name'),
            'search_items'      => __('Search Categorias', 'template_name'),
            'all_items'         => __('Todas Categorias', 'template_name'),
            'parent_item_colon' => __('Categoria:', 'template_name'),
            'edit_item'         => __('Editar Categorias', 'template_name'),
            'update_item'       => __('Atualizar Categorias', 'template_name'),
            'add_new_item'      => __('Adicionar Nova Categoria', 'template_name'),
            'new_item_name'     => __('Novo nome de categorias', 'template_name'),
            'menu_name'         => __('Categorias', 'template_name')

        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'             => array('slug' => 'produto', 'with_front' => true, 'hierarchical' => true),
            'capabilities' => array(
                'manage_terms' => 'manage_products',
                'edit_terms' => 'edit_products',
                'delete_terms' => 'delete_products',
                'assign_terms' => 'assign_products',
            )
        );

        register_taxonomy('tipos-de-produtos', ['products'], $args);
    }
}

name_post_type::init();
