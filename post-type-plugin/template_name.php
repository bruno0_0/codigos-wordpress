<?php
/**
 *
 * Plugin Name: NOME DO SITE
 * Plugin URI: 
 * Description: produtos
 * Author: Bruno Almeida 
 * Version: 1.0.0
 * Author URI:
*/

class template_name
{
    private $version = '1.0';

    public function __construct()
    {
        $this->includes();
        add_action('admin_enqueue_scripts', [$this, 'load_scripts']);
    }

    public function includes()
    {
        require_once( plugin_dir_path( __FILE__ ) . 'PostTypes/name_post_type.php');
    }
}

new template_name();